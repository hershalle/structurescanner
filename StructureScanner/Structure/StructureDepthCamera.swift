//
//  StructureCamera.swift
//  Astralink
//
//  Created by Shai Balassiano on 21/01/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import Foundation
import MulticastDelegateSwift
import CoreMLHelpers

protocol StructureDepthCameraDelegate: class {
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didUpdateStatus status: StructureDepthCamera.ConnectionStatus)
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, batteryChargePercentage: Int, isLow: Bool)
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didInterruptWithStatus connectionStatus: StructureDepthCamera.ConnectionStatus)

    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didOutputSyncedFrames colorFrame: STColorFrame, depthFrame: STDepthFrame)
}

class StructureDepthCamera: NSObject {


    // MARK: - Types

    enum ConnectionStatus: String {
        case preparing
        case connected
        case failedToFind
        case failedToConnect
        case failedBackgroundNotSupported

        case disconnected

        init(structureInitStats: STSensorControllerInitStatus) {
            switch structureInitStats {
            case .openFailed:
                self = .failedToConnect
            case .appInBackground:
                self = .failedBackgroundNotSupported
            case .alreadyInitialized, .success:
                self = .connected
            case .sensorNotFound:
                self = .failedToFind
            case .sensorIsWakingUp:
                self = .preparing
            }
        }

        var description: String {
            switch self {
            case .connected:
                return "Initialized"
            case .failedToFind:
                return "Uninitialized because the sensor was not found."
            case .preparing:
                return "Uninitialized because sensor is waking up."
            case .failedToConnect:
                return "Failure to open the connection."
            case .failedBackgroundNotSupported:
                return "Uninitialized and sensor is not opened because the application is running in the background."
            case .disconnected:
                return "Sensor disconnected"
            }
        }
    }


    // MARK: - Private properties

    private struct ColorConfiguration {
        static let useManualExposureAndAutoISO: Bool = true
        static let targetExposureTimeInSeconds: Double = 1.0 / 60
        static let outputDataVideoSettings: [String: Int] = [String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        static let lensPosition: Float = 0.75
    }

    private var isStreaming = false
    private var shouldPerformStreaming = false

    private let multicastDelegate = MulticastDelegate<StructureDepthCameraDelegate>()


    // MARK: - Public properties

    static let shared = StructureDepthCamera()


    // MARK: - Initialization

    private override init() {
        super.init()

        STSensorController.shared().delegate = self

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appWillEnterForground),
                                               name: .UIApplicationWillEnterForeground ,
                                               object: nil)
    }

    private func initializeSensorConnection(completion: @escaping (ConnectionStatus) -> Void) {
        // Must be called within main thread since Structure performs a call to UIApplication.shared.applicationState
        DispatchQueue.main.async {
            let connectionStatus = ConnectionStatus(structureInitStats: STSensorController.shared().initializeSensorConnection())
            completion(connectionStatus)
        }
    }

    @objc private func appWillEnterForground() {
        if shouldPerformStreaming {
            startStreaming()
        }
    }


    // MARK: - Public API

    func connect(completion: @escaping (ConnectionStatus) -> Void) {
        initializeSensorConnection { connectionStatus in
            completion(connectionStatus)
        }
    }

    func batteryChargePercentage(completion: @escaping (_ percentage: Int) -> Void) {
        connect { (connectionStatus) in

            if connectionStatus == .connected {
                let percentage = Int(STSensorController.shared().getBatteryChargePercentage())
                completion(percentage)
            } else {
                AppLogger.error(connectionStatus.description)
                let unknownValue = -1
                completion(unknownValue)
            }
        }
    }

    func startStreaming() {
        shouldPerformStreaming = true
        guard !isStreaming else {return}

        connect { [weak self] connectionStatus in
            guard connectionStatus == .connected && !STSensorController.shared().isLowPower() else {
                AppLogger.error(connectionStatus)
                return
            }
            do {
                try STSensorController.shared().startStreaming(options: [
                    kSTStreamConfigKey: STStreamConfig.depth320x240.rawValue,
                    kSTFrameSyncConfigKey: STFrameSyncConfig.depthAndRgb.rawValue,
                    kSTColorCameraFixedLensPositionKey: ColorConfiguration.lensPosition])

                AppLogger.info()
                self?.isStreaming = true
            } catch let error {
                AppLogger.error(error.localizedDescription)
            }
        }
    }


    func stopStreaming() {
        STSensorController.shared().stopStreaming()
        isStreaming = false
        shouldPerformStreaming = false
    }

    func syncWithDeviceCamera(sampleBuffer: CMSampleBuffer) {
        _ = connect { _ in
            STSensorController.shared().frameSyncNewColorBuffer(sampleBuffer)
        }
    }

    func addObserver(_ observer: StructureDepthCameraDelegate) {
        multicastDelegate.addDelegate(observer)
    }

    func removeObserver(_ observer: StructureDepthCameraDelegate) {
        multicastDelegate.removeDelegate(observer)
        if multicastDelegate.isEmpty {
            stopStreaming()
        }
    }
}

extension StructureDepthCamera: STSensorControllerDelegate {
    internal func sensorDidConnect() {
        if shouldPerformStreaming {
            startStreaming()
        }
        AppLogger.info()
        multicastDelegate.invokeDelegates({ (delegate) in
            delegate.structureDepthCamera(self, didUpdateStatus: ConnectionStatus.connected)
        })
    }

    internal func sensorDidDisconnect() {
        AppLogger.info()
        isStreaming = false
        multicastDelegate.invokeDelegates({ (delegate) in
            delegate.structureDepthCamera(self, didUpdateStatus: ConnectionStatus.disconnected)
        })
    }

    internal func sensorDidStopStreaming(_ reason: STSensorControllerDidStopStreamingReason) {
        AppLogger.info()
        isStreaming = false
//        delegate?.structureDepthCamera(self, didInterruptWithStatus: ConnectionStatus.)
    }

    internal func sensorDidLeaveLowPowerMode() {
        AppLogger.info()
        multicastDelegate.invokeDelegates({ (delegate) in
            batteryChargePercentage { _ in

            }
            batteryChargePercentage { percentage in
                delegate.structureDepthCamera(self, batteryChargePercentage: percentage, isLow: false)
            }
        })
    }

    internal func sensorBatteryNeedsCharging() {
        AppLogger.info()
        multicastDelegate.invokeDelegates({ (delegate) in
            batteryChargePercentage { percentage in
                delegate.structureDepthCamera(self, batteryChargePercentage: percentage, isLow: true)
            }
        })
    }

    internal func sensorDidOutputSynchronizedDepthFrame(_ depthFrame: STDepthFrame!, colorFrame: STColorFrame!) {
        multicastDelegate.invokeDelegates({ (delegate) in
            delegate.structureDepthCamera(self,
                                          didOutputSyncedFrames: colorFrame,
                                          depthFrame: depthFrame.registered(to: colorFrame).copy() as! STDepthFrame)
        })
    }
}
