//
//  CoreMotionManager.swift
//  Astralink
//
//  Created by Shai Balassiano on 24/01/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import Foundation
import MulticastDelegateSwift

protocol CoreMotionServiceDelegate: class {
    func coreMotionService(_ service: CoreMotionService, didUpdate motion: CMDeviceMotion)
}


/// From Apple: "An app should create only a single instance of the CMMotionManager class.
/// Multiple instances of this class can affect the rate at which data is received from the accelerometer and gyroscope."
class CoreMotionService {
    private var motionManager = CMMotionManager()
    private var imuQueue = OperationQueue()
    private var imuIsReady = false

    private(set) var lastMotionUpdate: CMDeviceMotion?

    static let shared = CoreMotionService()
    private init() {}

    private let multicastDelegate = MulticastDelegate<CoreMotionServiceDelegate>()

    func stopDeviceMotionUpdates(for delegate: CoreMotionServiceDelegate) {
        multicastDelegate.removeDelegate(delegate)
        if multicastDelegate.isEmpty {
            motionManager.stopDeviceMotionUpdates()
        }
    }

    func startDeviceMotionUpdates(delegate: CoreMotionServiceDelegate) {
        multicastDelegate.addDelegate(delegate)
        startIfNeeded()
    }

    private func startIfNeeded() {
        let isNeeded = !motionManager.isDeviceMotionActive
        guard isNeeded else {
            return
        }

        if !imuIsReady {
            setupIMU()
        }

        let motionHandler: CMDeviceMotionHandler = { (motion: CMDeviceMotion?, error: Error?) in
            guard !self.multicastDelegate.isEmpty else {
                self.motionManager.stopDeviceMotionUpdates()
                return
            }

            if let error = error {
                AppLogger.error(error)
            }

            guard let motion = motion else {
                return
            }

            self.lastMotionUpdate = motion
            self.multicastDelegate.invokeDelegates({ (delegate) in
                delegate.coreMotionService(self, didUpdate: motion)
            })
        }
        // We use X-arbitrary ref frame so that magnetometer corrections don't influence yaw
        motionManager.startDeviceMotionUpdates(using: .xArbitraryZVertical,
                                               to: imuQueue,
                                               withHandler: motionHandler)
        AppLogger.info("Motion manager started streaming...")
    }

    private func setupIMU() {
        /// Limiting the concurrent ops to 1 is a simple way to force serial execution
        imuQueue.maxConcurrentOperationCount = 1

        //lastGravityVec = GLKVector3Make (0, 0, 0)

        /// 60 FPS is responsive enough for motion events.
        let fps: Double = 60.0
        motionManager.accelerometerUpdateInterval = 1.0/fps
        motionManager.gyroUpdateInterval = 1.0/fps

        imuIsReady = true
    }

    func deviceIsVertical() -> Bool {
        let pitchEpsilon = 1.0
        guard let rollInRadians = lastMotionUpdate?.attitude.roll else {
            return false
        }
        let rollInDegrees = rollInRadians * 180 / .pi
        if rollInDegrees > -90 - pitchEpsilon && rollInDegrees < -90 + pitchEpsilon {
            return true
        }
        return false
    }
}
