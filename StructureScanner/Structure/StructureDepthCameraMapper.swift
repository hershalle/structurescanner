//
//  StructureDepthCameraMapper.swift
//  Astralink
//
//  Created by Ido Schragenheim on 25/02/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import Foundation

class StructureDepthCameraMapper {

    // MARK: - Private properties
    private let mapper: STMapper
    private var documentsDir: URL = {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return URL(fileURLWithPath: paths.first!)
    }()

    private var meshFileUrl: URL {
        let id = UUID().uuidString
        return documentsDir.appendingPathComponent("mesh_\(id).zip")
    }

    // MARK: - Public properties
    var mesh: STMesh {
        let mesh = STMesh(mesh: mapper.scene.lockAndGetMesh())!
        self.mapper.scene.unlockMesh()
        return mesh
    }


    // MARK: - Initialization
    init(mapper: STMapper) {
        self.mapper = mapper
    }

    // MARK: - Public API

    func updateWith(depthFrame: STDepthFrame, andCameraPose cameraPose: GLKMatrix4) {
        mapper.integrateDepthFrame(depthFrame, cameraPose: cameraPose)
    }

    func finalizeMesh() {
        mapper.finalizeTriangleMesh()
    }

    func writeMeshToFile() {
        do {
            try mesh.write(
                toFile: meshFileUrl.path,
                options: [kSTMeshWriteOptionFileFormatKey: STMeshWriteOptionFileFormat.objFileZip.rawValue])
            AppLogger.info()
        } catch let error {
            AppLogger.info(error)
        }
    }

    func reset() {
        mapper.reset()
    }
}
