//
//  StructureDepthCameraScene.swift
//  Astralink
//
//  Created by Shai Balassiano on 27/05/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import Foundation

struct StructureDepthCameraScene {
    private let scene: STScene
    private let context: EAGLContext
    
    init(context: EAGLContext = EAGLContext(api: .openGLES2)!) {
        self.context = context
        scene = STScene(context: context)
    }

    func renderMesh(cameraViewPoint: GLKMatrix4, cameraGLProjection: GLKMatrix4) {
        DispatchQueue.main.async {
            glClear(GLbitfield(GL_COLOR_BUFFER_BIT))
            self.scene.renderMesh(fromViewpoint: cameraViewPoint,
                                  cameraGLProjection: cameraGLProjection,
                                  alpha: 1.0,
                                  highlightOutOfRangeDepth: false,
                                  wireframe: true)
            self.context.presentRenderbuffer(Int(GL_RENDERBUFFER))
        }
    }

    func clear() {
        scene.clear()
    }

    func constructTrackerThread() -> StructureDepthCameraTrackerThread {
        return StructureDepthCameraTrackerThread(scene: scene)
    }

    func constructMapper(configuration: [AnyHashable: Any]) -> StructureDepthCameraMapper {
        return StructureDepthCameraMapper(mapper: STMapper(scene: scene, options: configuration))
    }
}
