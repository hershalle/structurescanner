//
//  Tracker.swift
//  Astralink
//
//  Created by Ido Schragenheim on 04/07/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import Foundation


protocol StructureDepthCameraTrackerDelegate: class {
    func trackerPoseChanged(newPose: GLKMatrix4, depthFrame: STDepthFrame)
}

class StructureDepthCameraTracker {
    // MARK: - Private properties
    private var trackerThread: StructureDepthCameraTrackerThread?
    private let timeoutSeconds = 20.0 / 1000.0

    // MARK: - Public properties

    weak var delegate: StructureDepthCameraTrackerDelegate?

    var initialPose: GLKMatrix4? {
        return trackerThread?.tracker.initialCameraPose
    }
    
    // MARK: - Initialization
    deinit {
        stop()
    }


    // MARK: - Public API

    func start(initialPose: GLKMatrix4, scene: StructureDepthCameraScene) {
        if trackerThread == nil {
            trackerThread = scene.constructTrackerThread()
            trackerThread?.delegate = self
        }
        trackerThread?.setInitialTrackerPose(pose: initialPose, timestamp: CACurrentMediaTime())
        trackerThread?.start()
    }

    func reset() {
        trackerThread?.reset()
    }

    func stop() {
        trackerThread?.stop()
        trackerThread = nil
    }

    func updateWithMotionData(_ motion: CMDeviceMotion) {
        trackerThread?.updateWith(motion: motion)
    }

    func updateWithSyncedFrames(depthFrame: STDepthFrame, colorFrame: STColorFrame) {
        trackerThread?.updateWith(depthFrame: depthFrame, andColorFrame: colorFrame, maxWaitTimeInSec: timeoutSeconds)
    }
}

extension StructureDepthCameraTracker: StructureDepthCameraTrackerThreadDelegate {
    func trackerPoseChanged(newPose: GLKMatrix4, depthFrame: STDepthFrame) {
        delegate?.trackerPoseChanged(newPose: newPose, depthFrame: depthFrame)
    }
}
