//
//  TrackerThreadNew.swift
//  Astralink
//
//  Created by Ido Schragenheim on 10/07/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import Foundation

protocol StructureDepthCameraTrackerThreadDelegate: class {

    func trackerPoseChanged(newPose: GLKMatrix4, depthFrame: STDepthFrame)
}

class StructureDepthCameraTrackerThread {

    enum TrackerCommandAction {
        case none
        case reset
        case setInitialPose
        case processNextFrame
    }

    struct TrackerCommand {

        var action: TrackerCommandAction = .none

        var finished: Bool {
            return action == .none || isProcessed
        }

        var depthFrame: STDepthFrame!
        var colorFrame: STColorFrame!

        var cameraPose: GLKMatrix4!

        var timestamp: TimeInterval = -1

        var isProcessed = false
    }

    struct TrackerUpdate {
        var timestamp = -1.0
        var cameraPose = GLKMatrix4MakeScale(Float.nan, Float.nan, Float.nan)
        var couldEstimatePose = false
        var trackingError: Error?
        var trackerHints: STTrackerHints = STTrackerHints(trackerIsLost: true, sceneIsTooClose: false, modelOutOfView: false)
        var poseAccuracy: STTrackerPoseAccuracy = .notAvailable
    }

    var tracker: STTracker!
    var trackerThread: Thread!

    var nextCommand: TrackerCommand = TrackerCommand()
    var nextCommandChangedCondition: NSCondition = NSCondition()

    var lastUpdate: TrackerUpdate = TrackerUpdate()
    var lastUpdateChangedCondition: NSCondition = NSCondition()

    var threadPriority = 0.8

    weak var delegate: StructureDepthCameraTrackerThreadDelegate?

    init(scene: STScene) {
        trackerThread = Thread(target: self, selector: #selector(StructureDepthCameraTrackerThread.run), object: nil)
        trackerThread.name = "tracker"
        trackerThread.threadPriority = threadPriority

        let trackerOptions: [AnyHashable: Any] = [
            kSTTrackerTypeKey: STTrackerType.depthAndColorBased.rawValue,
            kSTTrackerBackgroundProcessingEnabledKey: true,
            kSTTrackerAvoidPitchRollDriftKey: true,
            kSTTrackerAvoidHeightDriftKey: false
        ]
        tracker = STTracker(scene: scene, options: trackerOptions)
    }

    deinit {
        stop()
        trackerThread = nil
        tracker = nil
    }


    ///////////////////////////////////// Thread related methods ////////////////////////////////////

    @objc func run() {
        while !trackerThread.isCancelled {
            autoreleasepool(invoking: {
                nextCommandChangedCondition.lock()

                if nextCommand.finished {
                    nextCommandChangedCondition.wait() // wait till there is a command to process
                }

                var currentCommand: TrackerCommand = nextCommand

                // reset next command
                nextCommand = TrackerCommand()
                nextCommand.isProcessed = true
                nextCommandChangedCondition.signal()
                nextCommandChangedCondition.unlock()

                // Nothing to process? Let's wait for the next command.
                if !currentCommand.isProcessed {

                    switch currentCommand.action {

                    case .none:
                        break

                    case .reset:
                        tracker?.reset()
                        lastUpdateChangedCondition.lock()
                        lastUpdate = TrackerUpdate()
                        lastUpdateChangedCondition.signal()
                        lastUpdateChangedCondition.unlock()

                    case .setInitialPose:

                        // update tracker initial position
                        tracker.initialCameraPose = currentCommand.cameraPose

                        AppLogger.info("Setting Tracker initial pose to: \(tracker.initialCameraPose)")

                        lastUpdateChangedCondition.lock()

                        lastUpdate = TrackerUpdate()
                        lastUpdate.cameraPose = currentCommand.cameraPose!
                        lastUpdate.timestamp = currentCommand.timestamp
                        lastUpdate.couldEstimatePose = true
                        lastUpdate.trackerHints = STTrackerHints(trackerIsLost: true, sceneIsTooClose: false, modelOutOfView: false)
                        lastUpdate.poseAccuracy = .notAvailable
                        lastUpdate.trackingError = nil

                        lastUpdateChangedCondition.signal()
                        lastUpdateChangedCondition.unlock()

                    case .processNextFrame:
                        estimateNewPose(currentCommand: currentCommand)
                        currentCommand.colorFrame = nil
                        currentCommand.depthFrame = nil
                    }

                }
                currentCommand.isProcessed = true
            })
        }
    }

    func start() {
        if trackerThread.isExecuting {
            return
        }
        trackerThread.start()

        // We could use a condition here. We just want to wait until the thread finished.
        while !trackerThread.isExecuting {
            Thread.sleep(forTimeInterval: 0.005)// sleep for 5 ms.
        }
    }

    func stop() {
        guard trackerThread.isExecuting else {
            return
        }
        trackerThread.cancel()
        do {
            nextCommandChangedCondition.lock()
            nextCommand.action = .none
            nextCommandChangedCondition.signal()
            nextCommandChangedCondition.unlock()
        }
        if trackerThread.isFinished {
            return
        }
        // We could use a condition here. We just want to wait until the thread finished.
        while !trackerThread.isFinished {
            Thread.sleep(forTimeInterval: 0.005)
        }
    }

    func reset() {
        nextCommandChangedCondition.lock()

        while !nextCommand.finished {
            nextCommandChangedCondition.wait()
        }

        nextCommand = TrackerCommand()
        nextCommand.action = .reset

        nextCommandChangedCondition.signal()
        nextCommandChangedCondition.unlock()
    }

    ///////////////////////////////////// Tracker related methods ////////////////////////////////////

    func setInitialTrackerPose(pose: GLKMatrix4, timestamp: Double) {
        nextCommandChangedCondition.lock()

        // Make sure the previous frame is processed.
        while !nextCommand.finished {
            nextCommandChangedCondition.wait()
        }

        nextCommand.action = .setInitialPose
        nextCommand.cameraPose = pose
        nextCommand.isProcessed = false
        nextCommand.depthFrame = nil
        nextCommand.colorFrame = nil
        nextCommand.timestamp = timestamp

        nextCommandChangedCondition.signal()
        nextCommandChangedCondition.unlock()
    }

    func updateWith(depthFrame: STDepthFrame, andColorFrame colorFrame: STColorFrame, maxWaitTimeInSec: Double) {
        nextCommandChangedCondition.lock()

        // Make sure the previous frame is processed.

        let timeoutDate: Date = Date(timeIntervalSinceNow: maxWaitTimeInSec)

        var didTimeout = false

        // wait until the current command is processed or a timeout accure
        while !didTimeout && !nextCommand.finished {
            didTimeout = nextCommandChangedCondition.wait(until: timeoutDate) == false
        }

        if didTimeout {
            nextCommandChangedCondition.unlock()
            AppLogger.info("did timeout")
        }

        nextCommand.action = .processNextFrame
        nextCommand.isProcessed = false
        nextCommand.timestamp = depthFrame.timestamp
        nextCommand.depthFrame = depthFrame
        nextCommand.colorFrame = colorFrame

        nextCommandChangedCondition.signal()
        nextCommandChangedCondition.unlock()
    }

    func updateWith(motion: CMDeviceMotion) {
        tracker.updateCameraPose(with: motion)
    }

    func estimateNewPose(currentCommand command: TrackerCommand) {

        var trackerError: Error? = nil

        var newTrackerUpdate: TrackerUpdate = TrackerUpdate()
        newTrackerUpdate.timestamp = command.depthFrame.timestamp

        //let preTrackerCompute: Double = nowInSeconds() // for debug

        // First try to estimate the 3D pose of the new frame.
        do {
            try tracker.updateCameraPose(with: command.depthFrame, colorFrame: command.colorFrame)
        } catch let error {
            AppLogger.error(error)
            trackerError = error
            newTrackerUpdate.couldEstimatePose = false
            return
        }

        //let postTrackerCompute: Double = nowInSeconds()

        //for debugging tracker compute performance
        //        double lastTrackerComputeInterval = postTrackerCompute - preTrackerCompute
        //        [self debugTrackerComputeInterval:lastTrackerComputeInterval]

        newTrackerUpdate.trackerHints = tracker.trackerHints
        newTrackerUpdate.poseAccuracy = tracker.poseAccuracy
        newTrackerUpdate.trackingError = trackerError

        // When the quality was poor, we could still get a pose.
        newTrackerUpdate.couldEstimatePose = true
        newTrackerUpdate.cameraPose = tracker.lastFrameCameraPose()

        if newTrackerUpdate.poseAccuracy == .high {
            delegate?.trackerPoseChanged(newPose: newTrackerUpdate.cameraPose, depthFrame: command.depthFrame)
        } else {
            if newTrackerUpdate.trackerHints.sceneIsTooClose {
                AppLogger.warning("newTrackerUpdate.trackerHints.sceneIsTooClose == \(newTrackerUpdate.trackerHints.sceneIsTooClose)")
            }
            if newTrackerUpdate.trackerHints.trackerIsLost {
                AppLogger.warning("newTrackerUpdate.trackerHints.trackerIsLost == \(newTrackerUpdate.trackerHints.trackerIsLost)")
            }
        }

        lastUpdateChangedCondition.lock()
        lastUpdate = newTrackerUpdate
        lastUpdateChangedCondition.signal()
        lastUpdateChangedCondition.unlock()
    }

    func waitForUpdateMoreRecent(then timestamp: TimeInterval, maxWaitTimeInSeconds waitTime: Double) -> TrackerUpdate {

        lastUpdateChangedCondition.lock()

        let timeoutDate: Date = Date(timeIntervalSinceNow: waitTime)
        var didTimeout = false

        while !didTimeout && lastUpdate.timestamp < (timestamp + 1e-7) {
            didTimeout = lastUpdateChangedCondition.wait(until: timeoutDate) == false
        }

        lastUpdateChangedCondition.unlock()

        return lastUpdate
    }

    func nowInSeconds() -> Double {
        return CACurrentMediaTime()
    }

}
