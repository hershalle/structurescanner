//
//  AVCameraViewController.swift
//  Astralink
//
//  Created by Shai Balassiano on 23/01/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import UIKit

class ColorCameraViewController: UIViewController {

    let cameraController = ColorCameraController.shared
    var previewLayer: AVCaptureVideoPreviewLayer?

    override func viewDidLoad() {
        super.viewDidLoad()

        cameraController.configure()
        previewLayer = cameraController.previewLayerInstance()
        view.layer.addSublayer(previewLayer!)
        previewLayer?.frame = view.frame
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        previewLayer?.frame = view.frame
    }
}
