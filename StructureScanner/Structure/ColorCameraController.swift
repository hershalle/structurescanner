//
//  AVCameraViewController.swift
//  Astralink
//
//  Created by Shai Balassiano on 23/01/2018.
//  Copyright © 2018 Astralink. All rights reserved.
//

import UIKit
import MulticastDelegateSwift

protocol ColorCameraControllerDelegate: class {
    func colorCameraController(_ colorCameraController: ColorCameraController, didOutput sampleBuffer: CMSampleBuffer)
}

class ColorCameraController: NSObject {
    // MARK: - Private Types

    private struct CameraConfig {
        static let useManualExposureAndAutoISO: Bool = true
        static let targetExposureTimeInSeconds: Double = 1.0/60
        static let outputDataVideoSettings: [String: Int] = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        static let lensPosition: Float = 0.75
    }

    // MARK: - Private Properties

    private let captureSession = AVCaptureSession()
    private var rearCameraInput: AVCaptureInput?
    private var captureDevice: AVCaptureDevice?

    private var isConfigured = false


    // MARK: - Public Properties
    private let observers = MulticastDelegate<ColorCameraControllerDelegate>()
    static let shared = ColorCameraController()

    // MARK: - Initialization

    private override init() {}


    // MARK: - Public API

    func authorizationStatus() -> AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatus(for: .video)
    }

    func configure() {
        guard !isConfigured else {return}

        captureSession.beginConfiguration()

        let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back)

        let captureOutputData = AVCaptureVideoDataOutput()
        captureOutputData.videoSettings = CameraConfig.outputDataVideoSettings
        let captureSessionQueue = DispatchQueue(label: "CameraSessionQueue", attributes: [])
        captureOutputData.setSampleBufferDelegate(self, queue: captureSessionQueue)

        for captureDevice in session.devices {
            self.captureDevice = captureDevice
            self.rearCameraInput = try! AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(self.rearCameraInput!)
            captureSession.addOutput(captureOutputData)
        }

        captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
        captureSession.commitConfiguration()
        isConfigured = true
    }

    func start() {
        guard !captureSession.isRunning else {return}
        configure()
        captureSession.startRunning()
        AppLogger.info()
    }

    func avCaptureVideoOrientation(from deviceOrientation: UIDeviceOrientation) -> AVCaptureVideoOrientation {
        switch deviceOrientation {
        case .landscapeLeft:
            return .landscapeRight
        case .landscapeRight:
            return .landscapeLeft
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .unknown, .faceUp, .faceDown:
            return .landscapeRight
        }
    }

    func previewLayerInstance() -> AVCaptureVideoPreviewLayer {
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = avCaptureVideoOrientation(from: UIDevice.current.orientation)
        return previewLayer
    }

    func lockColorCamera(exposure shouldLockExposure: Bool,
                         whiteBalance shouldLockWhiteBalance: Bool,
                         andFocus shouldLockFocus: Bool) {
        do {
            try captureDevice?.lockForConfiguration()
        } catch let error {
            AppLogger.error(error)
        }

        /// If the manual exposure option is enabled, we've already locked exposure permanently, so do nothing here.
        if shouldLockExposure {
            if CameraConfig.useManualExposureAndAutoISO {
                /// locks the video device to 1/60th of a second exposure time
                setManualExposureAndAutoISO(videoDevice: captureDevice!,
                                            exposureTimeInSec: CameraConfig.targetExposureTimeInSeconds)

            } else {
                AppLogger.info("Locking Camera Exposure")

                /// Exposure locked to its current value.
                if captureDevice!.isExposureModeSupported(.locked) {
                    captureDevice?.exposureMode = .locked
                }
            }

        } else {
            AppLogger.info("Unlocking Camera Exposure")
            /// Auto-exposure
            captureDevice?.exposureMode = .continuousAutoExposure
        }

        /// Lock in the white balance here
        if shouldLockWhiteBalance {
            /// White balance locked to its current value.
            if captureDevice!.isWhiteBalanceModeSupported(.locked) {
                captureDevice?.whiteBalanceMode = .locked
            }
        } else {
            /// Auto-white balance.
            captureDevice?.whiteBalanceMode = .continuousAutoWhiteBalance
        }

        /// Lock focus
        if shouldLockFocus {
            /// Set focus at the 0.75 to get the best image quality for mid-range scene
            captureDevice?.setFocusModeLocked(lensPosition: CameraConfig.lensPosition, completionHandler: nil)

        } else {
            captureDevice?.focusMode = .continuousAutoFocus
        }
        captureDevice?.unlockForConfiguration()
    }

    func addObserver(_ observer: ColorCameraControllerDelegate) {
        observers.addDelegate(observer)
    }

    func removeObserver(_ observer: ColorCameraControllerDelegate) {
        observers.removeDelegate(observer)
        if observers.isEmpty {
            captureSession.stopRunning()
        }
    }


    // MARK: - Private API

    private func setManualExposureAndAutoISO(videoDevice: AVCaptureDevice, exposureTimeInSec: Double) {
        var targetExposureTime = CMTimeMakeWithSeconds(exposureTimeInSec, 1000)
        let currentExposureTime = videoDevice.exposureDuration
        let exposureFactor = CMTimeGetSeconds(currentExposureTime) / exposureTimeInSec

        let minExposureTime = videoDevice.activeFormat.minExposureDuration

        if( CMTimeCompare(minExposureTime, targetExposureTime) > 0 /* means Time1 > Time2 */ ) {
            /// if minExposure is longer than targetExposure, increase our target
            targetExposureTime = minExposureTime
        }

        let currentISO: Float = videoDevice.iso
        var targetISO: Float = Float(currentISO) * Float(exposureFactor)
        let maxISO: Float = videoDevice.activeFormat.maxISO
        let minISO: Float = videoDevice.activeFormat.minISO

        /// Clamp targetISO to [minISO ... maxISO]
        targetISO = targetISO > maxISO ? maxISO : targetISO < minISO ? minISO : targetISO

        videoDevice.setExposureModeCustom(duration: targetExposureTime, iso: targetISO, completionHandler: nil)
    }
}


// MARK: - Extensions

extension ColorCameraController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        observers.invokeDelegates { observer in
            observer.colorCameraController(self, didOutput: sampleBuffer)
        }
    }
}
