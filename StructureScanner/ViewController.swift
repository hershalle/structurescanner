//
//  ViewController.swift
//  StructureStarter
//
//  Created by Shai Balassiano on 21/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private var meshView: GLKView!
    private let context = EAGLContext(api: .openGLES2)!
    private var scene: StructureDepthCameraScene!

    private var colorCameraViewController: ColorCameraViewController!

    private var mapper: StructureDepthCameraMapper?
    private var cameraPoseInitializer: STCameraPoseInitializer!
    private let tracker = StructureDepthCameraTracker()
    private var slamData = SLAMData()

    private var isInitializingCameraPose = false

    struct SLAMData {
        var initialTrackerPose: GLKMatrix4?
        var lastSyncedFrames: (colorFrame: STColorFrame, depthFrame: STDepthFrame)?
        var lastMotionUpdate: CMDeviceMotion?
        var lastCameraPose: GLKMatrix4?
    }

    struct Configuration {
        static let `default` = Configuration(faceResolution: 0.03, volumeSizeInMeters: GLKVector3(v: (6.0, 4.0, 6.0)))

        var faceResolution: Float
        var volumeSizeInMeters: GLKVector3
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let colorCameraViewController = segue.destination as? ColorCameraViewController {
            self.colorCameraViewController = colorCameraViewController
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        scene = StructureDepthCameraScene(context: context)
        meshView.backgroundColor = UIColor.clear
        meshView.context = context
        CoreMotionService.shared.startDeviceMotionUpdates(delegate: self)
        tracker.delegate = self

        constructMapper()
        constructCameraPoseInitalizer(volumeSizeInMeters: Configuration.default.volumeSizeInMeters)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        startStreaming()
    }

    @IBAction func didTap(scanButton: UIButton) {
        startMapping()
    }

    @IBAction func didTap(resetButton: UIButton) {
        endMapping()
    }

    func startStreaming() {
        StructureDepthCamera.shared.startStreaming()
        StructureDepthCamera.shared.addObserver(self)
        colorCameraViewController.cameraController.start()
        colorCameraViewController.cameraController.addObserver(self)
    }

    func startMapping() {
        startTracking()
    }

    func endMapping() {

    }

    func startTracking(initialPose: GLKMatrix4? = nil) {
        guard let initialTrackerPose = initialPose ?? (cameraPoseInitializer.hasValidPose ? cameraPoseInitializer.cameraPose : nil) else {
            isInitializingCameraPose = true
            return
        }
        isInitializingCameraPose = false
        colorCameraViewController.cameraController.lockColorCamera(exposure: true, whiteBalance: true, andFocus: true)
        tracker.start(initialPose: initialTrackerPose, scene: scene)

    }

    func constructMapper(configuration: Configuration = .default) {
        let volumeSizeVec = GLKVector3DivideScalar(configuration.volumeSizeInMeters, configuration.faceResolution) // translate meters to number of faces
        let structureMappingConfiguration: [String: Any] = [
            kSTMapperVolumeResolutionKey: configuration.faceResolution,
            kSTMapperVolumeBoundsKey: [volumeSizeVec.x, volumeSizeVec.y, volumeSizeVec.z],
            kSTMapperEnableLiveWireFrameKey: true
        ]

        mapper = scene.constructMapper(configuration: structureMappingConfiguration)
    }

    func constructCameraPoseInitalizer(volumeSizeInMeters: GLKVector3) {
        let cameraPoseInitializerOptions = [kSTCameraPoseInitializerStrategyKey: STCameraPoseInitializerStrategy.gravityAlignedAtVolumeCenter.rawValue]
        cameraPoseInitializer = STCameraPoseInitializer(volumeSizeInMeters: volumeSizeInMeters, options: cameraPoseInitializerOptions)
    }

    private func renderMesh() {
        DispatchQueue.main.async {
            AppLogger.info()
            let depthCameraPose = self.slamData.lastCameraPose!
            let cameraGLProjection = self.slamData.lastSyncedFrames?.colorFrame.glProjectionMatrix()
            let colorCamInDepthCoordinateSpace = UnsafeMutablePointer<Float>.allocate(capacity: 16)
            self.slamData.lastSyncedFrames?.depthFrame.colorCameraPose(inDepthCoordinateFrame: colorCamInDepthCoordinateSpace)
            let cameraViewPoint = GLKMatrix4Multiply(depthCameraPose, GLKMatrix4MakeWithArray(colorCamInDepthCoordinateSpace))

            self.scene.renderMesh(cameraViewPoint: cameraViewPoint, cameraGLProjection: cameraGLProjection!)
        }
    }
}

extension ViewController: CoreMotionServiceDelegate {
    func coreMotionService(_ service: CoreMotionService, didUpdate motion: CMDeviceMotion) {
        func updateCameraPoseInitializerWithMotion(_ motion: CMDeviceMotion) {
            do {
                try cameraPoseInitializer.updateCameraPose(withGravity:
                    GLKVector3(v: (Float(motion.gravity.x),
                                   Float(motion.gravity.y),
                                   Float(motion.gravity.z))), depthFrame: nil)
            } catch let error {
                AppLogger.error(error)
            }
        }

        if isInitializingCameraPose {
            updateCameraPoseInitializerWithMotion(motion)
            startTracking()
        }

        slamData.lastMotionUpdate = motion
        tracker.updateWithMotionData(motion)
    }
}

extension ViewController: StructureDepthCameraDelegate {
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didUpdateStatus status: StructureDepthCamera.ConnectionStatus) {
        AppLogger.debug(status.description)
    }

    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, batteryChargePercentage: Int, isLow: Bool) {}

    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didInterruptWithStatus connectionStatus: StructureDepthCamera.ConnectionStatus) {}

    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didOutputSyncedFrames colorFrame: STColorFrame, depthFrame: STDepthFrame) {
        slamData.lastSyncedFrames = (colorFrame, depthFrame)
        tracker.updateWithSyncedFrames(depthFrame: depthFrame, colorFrame: colorFrame)
    }
}

extension ViewController: ColorCameraControllerDelegate {
    func colorCameraController(_ colorCameraController: ColorCameraController, didOutput sampleBuffer: CMSampleBuffer) {
        StructureDepthCamera.shared.syncWithDeviceCamera(sampleBuffer: sampleBuffer)
    }
}

extension ViewController: StructureDepthCameraTrackerDelegate {
    func trackerPoseChanged(newPose: GLKMatrix4, depthFrame: STDepthFrame) {
        AppLogger.info()
        slamData.lastCameraPose = newPose
        mapper?.updateWith(depthFrame: depthFrame, andCameraPose: newPose)
        renderMesh()
    }
}
