//
//  AppLogger.swift
//  TestLogger
//
//  Created by Shai Balassiano on 16/05/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

/**
 Verbose
 Verbose is the noisiest level, generally used only as a last resort when debugging a difficult problem, and rarely (if ever) enabled for a production app.

 For example, local variables within an algorithm implementation might be logged at this level.

 Debug
 Debug is used for internal system events that are not necessarily observable from the outside, but useful when determining how something happened.

 For example, the details of requests and responses made to and from external integration points would often be logged at this level.

 Information
 Information events describe things happening in the system that correspond to its responsibilities and functions. Generally these are the observable actions the system can perform.

 For example, processing a payment or updating a user's details will be logged at this level.

 Warning
 When service is degraded, endangered, or may be behaving outside of its expected parameters, Warning level events are used.

 A warning event should only be emitted when the condition is either transient or can be investigated and fixed - use restraint to avoid polluting the log with spurious warnings.

 For example, slow response times from a critical database would be logged as warnings.

 Error
 When functionality is unavailable or expectations broken, an Error event is used.

 For example, receiving an exception when trying to commit a database transaction is an event at this level.

 Fatal
 The most critical level, Fatal events demand immediate attention.

 For example, an application failing during startup will log a Fatal event.

 If you have a pager, it goes off when one of these occurs.

 */
struct AppLogger {
    static func configure() {
        SwiftyBeaverWrapper.configureLogToConsole()
        SwiftyBeaverWrapper.configureLogToCloud()
    }
    /**
     Verbose is the noisiest level, generally used only as a last resort when debugging a difficult problem, and rarely (if ever) enabled for a production app.

     For example, local variables within an algorithm implementation might be logged at this level.
     */
    static func verbose(_ message: @autoclosure () -> Any = "", _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .verbose, message: message, file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(message)
    }

    /**
     Debug is used for internal system events that are not necessarily observable from the outside, but useful when determining how something happened.

     For example, the details of requests and responses made to and from external integration points would often be logged at this level.
     */
    static func debug(_ message: @autoclosure () -> Any = "", _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .debug, message: message, file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(message)
    }

    /**
     Information events describe things happening in the system that correspond to its responsibilities and functions. Generally these are the observable actions the system can perform.

     For example, processing a payment or updating a user's details will be logged at this level.
     */
    static func info(_ message: @autoclosure () -> Any = "", _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .info, message: message, file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(message)
    }

    /**
     When service is degraded, endangered, or may be behaving outside of its expected parameters, Warning level events are used.

     A warning event should only be emitted when the condition is either transient or can be investigated and fixed - use restraint to avoid polluting the log with spurious warnings.

     For example, slow response times from a critical database would be logged as warnings.
     */
    static func warning(_ message: @autoclosure () -> Any = "", _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .warning, message: message, file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(message)
    }

    /**
     When functionality is unavailable or expectations broken, an Error event is used.
     */
    static func error(_ message: @autoclosure () -> Any = "", _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .error, message: message, file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(message)
    }

    /**
     When functionality is unavailable or expectations broken, an Error event is used.
     */
    static func log(error: @escaping @autoclosure () -> Error, _
        file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        SwiftyBeaverWrapper.logger.custom(level: .error, message: error(), file: file, function: function, line: line, context: context)

//        CrashlyticsManager.shared.log(error())
    }

    static func test() {
        // log with different importance
        log(error: NSError(domain: "domain one", code: 100, userInfo: ["info": "infoValue"]))
        verbose("verbose: not so important")  // prio 1, VERBOSE in silver
        debug("debug: something to debug")  // prio 2, DEBUG in blue
        info("info: a nice information")   // prio 3, INFO in green
        warning("warning: oh no, that won’t be good")  // prio 4, WARNING in yellow
        self.error("error: ouch, an error did occur!")  // prio 5, ERROR in red

        // log strings, ints, dates, etc.
        log(error: NSError(domain: "domain", code: -1, userInfo: nil))
        verbose(123)
        info(-123.45678)
        warning(NSDate())
        self.error(["I", "like", "logs!"])
        self.error(["name": "Mr Beaver", "address": "7 Beaver Lodge"])
    }
}
