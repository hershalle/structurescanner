//
//  SwiftyBeaverWrapper.swift
//  TestLogger
//
//  Created by Shai Balassiano on 21/05/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation
import SwiftyBeaver

struct SwiftyBeaverWrapper {
    static let logger = SwiftyBeaver.self
    private static let format = "$DHH:mm:ss.SSS$d $C$L$c $N.$F:$l - $M"

    static func configureLogToConsole() {
        let console = ConsoleDestination()  // log to Xcode Console
        console.format = format
        console.levelColor.debug = "🏗️ "
        console.levelColor.error = "❌ "
        console.levelColor.info = "ℹ️ "
        console.levelColor.verbose = "🏗️📢 "
        console.levelColor.warning = "⚠️ "
        logger.addDestination(console)
    }

    static func configureLogToFile() {
        let file = FileDestination()  // log to default swiftybeaver.log file
        logger.addDestination(file)
    }

    static func configureLogToCloud() {
        let cloud = SBPlatformDestination(appID: "PVnwqP", appSecret: "CakZqiQdakQ4cdlut8orIt9llkewsssu", encryptionKey: "djjj0fzFrmhbaotexxxttijzsztlSgr4") // to cloud

        cloud.format = format
        cloud.showNSLog = false // debug for SwiftyBeaver

        logger.addDestination(cloud)
    }
    
}
